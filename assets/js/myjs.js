$(function(){
    $(".openBtn").click(function(){
        $(".dropdown").toggleClass('active')
    })
    $(".menu-closeBtn").click(function(){
        $(".dropdown").toggleClass('active')
    })
    
})

$(".tabs li").click(function () {  
    $(".tabs li").removeClass('active') 
    $(this).addClass('active')
    let selected_category = $(this).attr("id");
    if (selected_category == "all") {
      $("[data-category]").show();
    } else {
      $("[data-category]").hide();
      $('[data-category="' + selected_category + '"]').show();
    }
  });
  // Smooth Scrolling
$(function () {

  $("menus li a.smooth-scroll").click(function (event) {

      event.preventDefault();

      // get section id like #about, #servcies, #work, #team and etc.
      var section_id = $(this).attr("href");

      $("html, body").animate({
          scrollTop: $(section_id).offset().top - 64
      }, 1250, "easeInOutExpo");

  });
  $(".playIcon").on('click', function(){
    $(this).hide();
    $(this).parent().find(".poster").hide();
    $(this).parent().find(".pauseIcon").show();
    let video = $(this).parent().find("video")[0]; 
    video.play();
});


$(".pauseIcon").on('click', function(){
    $(this).hide();
    $(this).parent().find(".poster").show();
    $(this).parent().find(".playIcon").show();
    let video = $(this).parent().find("video")[0]; 
    video.pause();
});

});
  
